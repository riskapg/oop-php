<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Animal name : $sheep->name <br>"; // "shaun"
echo "legs : $sheep->legs <br>"; // 2
echo "cold Blood: $sheep->cold_blooded <br> <br>";// false

$kodok = new Frog("buduk");

echo "Animal name : $kodok->name <br>"; // "buduk"
echo "legs : $kodok->legs <br>"; // 4
echo "cold Blood: $kodok->cold_blooded <br>";// false
$kodok->jump(); // "hop hop"

echo "<br><br>";

$sungokong = new Ape("kera sakti");

echo "Animal name : $sungokong->name <br>"; // "kera sakti"
echo "legs : $sungokong->legs <br>"; // 4
echo "cold Blood: $sungokong->cold_blooded <br>";// false
$sungokong->yell(); // "Auooo"

